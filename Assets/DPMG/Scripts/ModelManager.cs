﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelManager : MonoBehaviour {

	public Camera m_camera;
	public GameObject[] models;
	public GameObject changeModeBtn;
	public Sprite moveImage, rotateImage;

	private int count = 2;
	private int cur;
	private int prevTouchCount;
	private bool rotateOrMove = false, isRotate, isMove, isScale;
	private Vector2 prevPos, prevPos0, prevPos1;

	// Use this for initialization
	void Start () {
		cur = 0;
		models [1].SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 1) {
			Touch touch0 = Input.GetTouch (0);
			if (prevTouchCount == 0) {
				if (rotateOrMove)
					isRotate = true;
				else
					isMove = true;
				prevPos = touch0.position;
			}
			if (isMove) {
				Ray ray = m_camera.ScreenPointToRay (prevPos);
				Vector3 prev = ray.GetPoint (30);
				ray = m_camera.ScreenPointToRay (touch0.position);
				Vector3 newPos = ray.GetPoint (30);
				models[cur].transform.position += newPos - prev;
			}
			if (isRotate) {
				float dx = touch0.position.x - prevPos.x;
				float dy = touch0.position.y - prevPos.y;
				models[cur].transform.Rotate (new Vector3 (dy * 0.5f, 0, 0), Space.World);
			}
			prevPos = touch0.position;
		} else
			isMove = isRotate = false;
		if (Input.touchCount == 2) {
			Touch touch0 = Input.GetTouch (0);
			Touch touch1 = Input.GetTouch (1);
			if (prevTouchCount != 2) {
				isScale = true;
				prevPos0 = touch0.position;
				prevPos1 = touch1.position;
			}
			if (isScale) {
				Vector2 pos0 = touch0.position;
				Vector2 pos1 = touch1.position;
				Vector3 scale = models[cur].transform.localScale;
				scale *= (pos0 - pos1).magnitude / (prevPos0 - prevPos1).magnitude;
				models[cur].transform.localScale = scale;
				prevPos0 = pos0;
				prevPos1 = pos1;
			}
		} else {
			isScale = false;
		}

		prevTouchCount = Input.touchCount;
	}

	public void OnChangeCameraButton() {
	}

	public void OnChangeModeButton() {
		rotateOrMove = !rotateOrMove;
		changeModeBtn.GetComponent<UnityEngine.UI.Image> ().sprite = rotateOrMove ? moveImage : rotateImage;
	}

	public void OnSelectWatch(int id) {
		if (cur != id) {
			models [cur].SetActive (false);
			cur = id;
			models [cur].SetActive (true);
		}
	}
}
