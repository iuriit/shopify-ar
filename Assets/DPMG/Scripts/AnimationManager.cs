﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationDirection
{
    LeftToRight = 0,
    RightToLeft = 1,
    TopToBottom = 2,
    BottomToTop = 3,
	None = 100,
}

[RequireComponent(typeof(Animator))]
public class AnimationManager : MonoBehaviour {

	public bool StartAtAwake = true;
    public AnimationDirection direction;
	public GameObject prevPage;

	Animator _animator;
	GameObject lastPage;

	void Start(){
		_animator = this.gameObject.GetComponent <Animator> ();
	}

	void OnEnable(){
		_animator = this.gameObject.GetComponent <Animator> ();
		if(StartAtAwake)
			StartAnimation ();
		if (!GameManager.Instance.isEscape)
			GameManager.Instance.SetCurrentPage (this.gameObject);
/*		else
			GameManager.Instance.isEscape = false;
*/	}

	public void StartAnimation(){

		if (this.gameObject.activeSelf)
			return;

		this.gameObject.SetActive (true);
		int dir = (int)direction;
		if (_animator == null)
			_animator = this.gameObject.GetComponent <Animator> ();
		_animator.SetInteger ("Direction", dir);

		this.transform.SetAsLastSibling ();
	}

	public void AnimationEnd(){
		_animator.SetInteger ("Direction", (int)AnimationDirection.None);
		if(!GameManager.Instance.isEscape)
			GameManager.Instance.GetPrevPage ().SetActive (false);
		else{
			GameManager.Instance.lastPage.SetActive (false);
			GameManager.Instance.isEscape = false;
		}
		/*if(lastPage != null)
			lastPage.SetActive (false);*/
	}
}
