﻿using System;
using System.Collections.Generic;
using System.Linq;
using Components;
using Shopify.Examples.Helpers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AddProductToCartEvent : UnityEvent<Shopify.Unity.Product, Shopify.Unity.ProductVariant> {
}

public class ProductManager : MonoBehaviour {

	public AddProductToCartEvent OnAddProductToCart = new AddProductToCartEvent();

	public Image ProductImage;
	public Text ProductPrice;
	public Button AddToCartButton;

	public Transform contentTransform;

	public Shopify.Unity.Product CurrentProduct;
	public Shopify.Unity.ProductVariant CurrentVariant;

	public ImageHolder ImageHolderTemplate;

	private readonly List<GameObject> _imageHolders = new List<GameObject>();

	// Use this for initialization
	void Start () {
		AddToCartButton.onClick.AddListener(() => {
			OnAddProductToCart.Invoke(CurrentProduct, CurrentVariant);
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowProduct(Shopify.Unity.Product product) {

		// Parse variant titles into a list of strings and assign to the dropdown as the new options
		var variants = (List<Shopify.Unity.ProductVariant>)product.variants();

		// Assign the first product image to the main product image display
		var images = (List<Shopify.Unity.Image>)product.images();
		StartCoroutine(ImageHelper.AssignImage(images[0].src(), ProductImage));

		RenderVariantImages(images);

		ProductPrice.text = variants[0].price().ToString("C");

		CurrentProduct = product;
		CurrentVariant = variants[0];
	}

	private void RenderVariantImages(List<Shopify.Unity.Image> images) {
		// Clean up the existing thumbnail images
		foreach (var imageHolder in _imageHolders) {
			Destroy(imageHolder);
		}
		_imageHolders.Clear();

		// We only have space for a fixed number of thumbnails, so we render out the first five
		var maxImages = 5;
		if (images.Count < maxImages) {
			maxImages = images.Count;
		}
		foreach (var image in images.GetRange(0, maxImages)) {
			// Generate instance of thumbail template
			var instance = Instantiate(ImageHolderTemplate);
			instance.gameObject.SetActive(true);
			_imageHolders.Add(instance.gameObject);
			var instanceImage = instance.ForegroundButton.GetComponent<Image>();
			StartCoroutine(
				ImageHelper.AssignImage(
					image.src(),
					instanceImage,
			 		instance.BrokenImageIcon
				)
			);
			instance.transform.SetParent(contentTransform, false);

			var instanceButton = instance.ForegroundButton.GetComponent<Button>();
			instanceButton.onClick.AddListener(() =>
				StartCoroutine(ImageHelper.AssignImage(image.src(), ProductImage))
			);
		}
	}
}
