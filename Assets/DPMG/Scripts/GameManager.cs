﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager Instance;
	[HideInInspector] public bool isEscape;

	void Awake(){
		Instance = this;
		DontDestroyOnLoad (this);
	}

	void Update(){
		if(Input.GetButtonDown ("Cancel")){
			if (GetPrevPage () != null) {
				RemoveLastHistory ();
				isEscape = true;
				GetLastPage().GetComponent <AnimationManager> ().StartAnimation ();
			}
			else
				Application.Quit ();
		}
	}


	#region Page Animation History

	List<GameObject> pageHistory = new List<GameObject> ();
	[HideInInspector] public GameObject lastPage;

	public void SetCurrentPage(GameObject currentPg){
		pageHistory.Add (currentPg);
		print (currentPg.name + ":" + pageHistory.IndexOf (currentPg));
		/*lastPage = currentPage;
		currentPage = currentPg;*/
	}

	public GameObject GetPrevPage(){
		//return currentPage.GetComponent <AnimationManager>().prevPage;
		if (pageHistory.Count <= 1)
			return null;
		return pageHistory[pageHistory.Count-2];
	}

	public GameObject GetLastPage(){
		return pageHistory[pageHistory.Count-1];
	}

	void RemoveLastHistory(){
		lastPage = GetLastPage ();
		pageHistory.RemoveAt (pageHistory.Count-1);
	}
	#endregion
}
