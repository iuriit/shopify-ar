﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionManager : MonoBehaviour {

	public GameObject content;
	public GameObject menupanel;
	public Transform scrollContent;
	public GameObject[] products;

	private int viewMode = 3; // 3 items per row

	// Use this for initialization
	void Start () {
		SetMode ();
	}
	
	// Update is called once per frame
	void Update () {

	}
	public void onMenuPressed()
	{
        menupanel.GetComponent<EasyTween>().OpenCloseObjectAnimation();

    }
	public void OnChangeViewMode(int mode) {
		viewMode = mode;
		SetMode ();
	}

	void SetMode() {
		Vector2 size = new Vector2 (800 / viewMode, 800 / viewMode);
		content.GetComponent<GridLayoutGroup> ().cellSize = size;

		int i, productCount = content.transform.childCount;

		for (i = 0; i < productCount; i++) {
			GameObject product = content.transform.GetChild (i).gameObject;
			product.SendMessage ("SetDisplayMode", viewMode, SendMessageOptions.DontRequireReceiver);
		}
	}
	public void onLiveshop()
	{
		Application.LoadLevel ("GeminoUI");
	}
	public void onCheckoutPage()
	{
		Application.LoadLevel ("CheckoutPage");
	}
	public void onProductPage()
	{
		Application.LoadLevel ("ProductPage");
	}
	public void filterProducts(int type)
	{
	/*	foreach (Transform child in scrollContent)
			GameObject.Destroy (child.gameObject);
		for (int i = 0; i < products.Length; i++) {
			Item a = products [i].GetComponent<Item> ();
			if(a.type == type)
			{
				GameObject obj = GameObject.Instantiate (products [i]);
				obj.transform.SetParent (scrollContent);
				obj.transform.localScale = Vector3.one;
			}
		}*/
	}
	public void sortProducts(int sort)
	{
		/*
		foreach (Transform child in scrollContent)
			GameObject.Destroy (child.gameObject);
		print ("sort: " + sort);
		for(int i = 0; i < products.Length; i ++)
		{
			Item a = products [i].GetComponent<Item> ();
			for(int j = i + 1; j < products.Length; j ++)
			{
				Item b = products [j].GetComponent<Item> ();
				print (a.price + ","+ b.price);
				if(sort == 0 && a.price > b.price)
				{
					print ("filter: " + sort);
					GameObject obj1 = GameObject.Instantiate (products [i]);
					GameObject obj2 = GameObject.Instantiate (products [j]);
					products [i] = obj2;
					products [j] = obj1;
					print ("products" + i + " : " + products [i]);
					products [i].SetActive (true);
				}
				if(sort == 1 && a.price < b.price)
				{
					GameObject obj1 = GameObject.Instantiate (products [i]);
					GameObject obj2 = GameObject.Instantiate (products [j]);
					products [i] = obj2;
					products [j] = obj1;
				}
				if(sort == 2 && string.Compare (a.time, b.time) >= 0)
				{
					GameObject obj1 = GameObject.Instantiate (products [i]);
					GameObject obj2 = GameObject.Instantiate (products [j]);
					products [i] = obj2;
					products [j] = obj1;
				}
				if(sort == 3 && string.Compare (a.time, b.time) <= 0)
				{
					GameObject obj1 = GameObject.Instantiate (products [i]);
					GameObject obj2 = GameObject.Instantiate (products [j]);
					products [i] = obj2;
					products [j] = obj1;
				}
				if(sort == 4 && a.review < b.review)
				{
					GameObject obj1 = GameObject.Instantiate (products [i]);
					GameObject obj2 = GameObject.Instantiate (products [j]);
					products [i] = obj2;
					products [j] = obj1;
				}
			}
		}
		for(int i = 0; i < products.Length; i ++)
		{
//			Item a = products [i].GetComponent<Item> ();
//			if (a.type == type) {
				GameObject obj = GameObject.Instantiate (products [i]);
				obj.transform.SetParent (scrollContent);
				obj.transform.localScale = Vector3.one;
//			}
		}*/
	}
}
