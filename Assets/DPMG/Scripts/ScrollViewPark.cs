﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollViewPark : MonoBehaviour, IEndDragHandler, IBeginDragHandler, IDragHandler {

    public ScrollRectEx scrollRect;
    public Transform scrollContent;

    public int childCount = 0;
    public int showCount = 0;
    public float moveTime = 0.3f;
    public bool isAutoRotate;
    public float autoRotateTime;
    public GameObject[] radioButtons;

    private bool isMove = false;
    private float timer = 0, autoTimer = 0;
    private float startPos, targetPos;
    private int curPos;
	private float prevPos;

    // Use this for initialization
    void Start () {
        if(isAutoRotate)
        {
            curPos = childCount;
            scrollContent.localPosition = new Vector3(-800 * childCount, scrollContent.localPosition.y, scrollContent.localPosition.z);
        }
    }

    // Update is called once per frame
    void Update () {
        if (isMove)
        {
            timer += Time.deltaTime;
            if (timer > moveTime)
            {
                isMove = false;
                scrollRect.horizontalNormalizedPosition = targetPos;
                scrollRect.inertia = true;
            }
            else
            {
                float value = timer / moveTime;
                //float value = Mathf.Sqrt(timer / moveTime);
                scrollRect.horizontalNormalizedPosition = Mathf.Lerp(startPos, targetPos, value);
            }
        }
        if(isAutoRotate)
        {
            if (scrollContent.localPosition.x > 1.5f * (-800))
                scrollContent.localPosition = new Vector3(scrollContent.localPosition.x - 800 * childCount, scrollContent.localPosition.y, scrollContent.localPosition.z);
            else if (scrollContent.localPosition.x < (childCount * 2 - showCount - 1.5f) * (-800))
                scrollContent.localPosition = new Vector3(scrollContent.localPosition.x + 800 * childCount, scrollContent.localPosition.y, scrollContent.localPosition.z);

            autoTimer += Time.deltaTime;
            if(autoTimer > autoRotateTime)
            {
                autoTimer = 0;
                OnMoveNext();
            }
        }
    }

    public void OnMoveNext()
    {
        int count = (isAutoRotate ? childCount * 2 : childCount);
        if (curPos < count - showCount - 1)
            moveTarget(curPos + 1);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
		if(isAutoRotate)
		{
			prevPos = scrollContent.localPosition.x;
		}
        autoTimer = 0;
        isMove = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        autoTimer = 0;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
		if(isAutoRotate)
		{
			float pos = scrollContent.localPosition.x;
			if (pos < prevPos - 100)
				moveTarget (curPos + 1);
			else if (pos > prevPos + 100)
				moveTarget (curPos - 1);
			else
				moveTarget (curPos);
			return;
		}
        if (childCount > showCount)
        {
            float dist = 2;
            int pos = 0;
            int count = (isAutoRotate ? childCount * 2 : childCount);
            for (int i = 0; i <= count - showCount; i++)
            {
                if (dist > Mathf.Abs(scrollRect.horizontalNormalizedPosition - (float)i / (count - showCount)))
                {
                    dist = Mathf.Abs(scrollRect.horizontalNormalizedPosition - (float)i / (count - showCount));
                    pos = i;
                }
            }
            moveTarget(pos);
        }
    }

    public void moveTarget(int pos)
    {
        int count = (isAutoRotate ? childCount * 2 : childCount);
        curPos = pos;
        if (isAutoRotate)
        {
            if (pos < 2)
                curPos += childCount;
            else if (pos > childCount * 2 - showCount - 2)
                curPos -= childCount;
        }
        startPos = scrollRect.horizontalNormalizedPosition;
        targetPos = (float)pos / (count - showCount);
        isMove = true;
        scrollRect.inertia = false;
        timer = 0;
        for (int i = 0; i < radioButtons.Length; i++)
            radioButtons[i].SetActive(i == (curPos % childCount));
    }

}
