﻿using System.Collections.Generic;
using System.Linq;
using Shopify.Examples.Helpers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour {

	public ProductManager productPage;

	public GameObject thumbnail;
	public Image ProductImage;
	public GameObject priceContainer;
	public GameObject currentPrice;
	public GameObject pastPrice;
	public GameObject strikeLine;
	public GameObject decreaseContainer;
	public GameObject decreaseBG;
	public GameObject decPerent;

	private string _imageSrc;

	private Shopify.Unity.Product currentProduct;

	[HideInInspector]

	public bool IsLoaded { get; private set; }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetDisplayMode(int mode) {
		switch (mode) {
		case 1:
			SetPosition (thumbnail, new Vector2 (0, -90));
			SetSize (thumbnail, new Vector2 (600, 600));
			SetPosition (priceContainer, new Vector2 (0, 35));
			SetWidth (currentPrice, 500);
			SetWidth (pastPrice, 500);
			SetWidth (decreaseContainer, 500);
			break;
		case 2:
			SetPosition (thumbnail, new Vector2 (0, -45));
			SetSize (thumbnail, new Vector2 (300, 300));
			SetPosition (priceContainer, new Vector2 (0, 0));
			SetWidth (currentPrice, 200);
			SetWidth (pastPrice, 200);
			SetWidth (decreaseContainer, 200);
			break;
		case 3:
			SetPosition (thumbnail, new Vector2 (0, -10));
			SetSize (thumbnail, new Vector2 (200, 200));
			SetPosition (priceContainer, new Vector2 (0, 5));
			SetWidth (currentPrice, 110);
			SetWidth (pastPrice, 110);
			SetWidth (decreaseContainer, 110);
			break;
		}
	}

	void SetPosition(GameObject obj, Vector2 pos) {
		RectTransform transform = (RectTransform)obj.transform;
		transform.anchoredPosition = pos;
	}

	void SetSize(GameObject obj, Vector2 size) {
		RectTransform transform = (RectTransform)obj.transform;
		transform.sizeDelta = size;
	}

	void SetWidth(GameObject obj, float width) {
		RectTransform transform = (RectTransform)obj.transform;
		Vector2 size = transform.sizeDelta;
		size.x = width;
		transform.sizeDelta = size;
	}

	public void SetCurrentProduct(Shopify.Unity.Product product) {

		currentProduct = product;

		gameObject.SetActive(true);

		var variants = (List<Shopify.Unity.ProductVariant>)product.variants();

		decimal current, past;
		current = variants.First ().price ();
	//	past = variants.First ().compareAtPrice ();
		currentPrice.GetComponent<Text> ().text = current.ToString ("C");
	//	pastPrice.GetComponent<Text> ().text = past.ToString ("C");
	//	decPerent.GetComponent<Text> ().text = ((past - current) / past).ToString ("P1");

		var images = (List<Shopify.Unity.Image>)product.images();

		if (images.Count > 0) {
			_imageSrc = images.First().src();

			StartCoroutine(
				ImageHelper.AssignImage(
					_imageSrc,
					ProductImage
				)
			);
		}
	}

	public void ShowProduct() {
		productPage.ShowProduct(currentProduct);
	}
}
