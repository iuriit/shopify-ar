﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginManager : MonoBehaviour {

    public GameObject WelcomeCanvas;
    public GameObject HomePageCanvas;
    public GameObject CollectionCanvas;
    public GameObject GeminoCanvas;
    public GameObject ProductCanvas;
    public GameObject CheckoutCanvas;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();		
		}
	}
	public void LoadScene()
	{
		HomePageCanvas.GetComponent <AnimationManager>().StartAnimation ();
	}

	public void onAddGuestPress()
	{
		HomePageCanvas.GetComponent <AnimationManager>().StartAnimation ();
	}
}
