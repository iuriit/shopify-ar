﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeManager : MonoBehaviour {

	// Use this for initialization
	public GameObject menupanel;
	public bool menubtnclicked = false;

	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.CancelQuit ();		
		}
	}


	public void OnLiveShop() {
		Application.LoadLevel ("GeminoUI");
	}
	public void OnCollectionPage(){
		Application.LoadLevel ("CollectionPage");
	}
	public void OnImagePress(int id)
	{
		GlobalInfo.manid = id;
		Application.LoadLevel ("ProductPage");
	}
	public void OnShoppingCartPress()
	{
		Application.LoadLevel ("CheckoutPage");
	}

	//	public bool hometab = false;
	//	public bool mentab = false;
	//	public bool womentab = false;
	//	public bool accessoriestab = false;
	//	public bool techtab = false;
	//	public bool mobilecasetab = false;
	//	public bool arttab = false;
	//	public bool bagstab = false;
	//	public bool otherstab = false;
	//	public bool accounttab = false;
	//	 Use this for initialization

	public void onMenuPressed()
	{
        menupanel.GetComponent<EasyTween>().OpenCloseObjectAnimation();

	}
	public void onHomeTabPressed()
	{
		Application.LoadLevel ("HomePage");
	}
	public void onAccountTabPressed()
	{
		Application.LoadLevel ("WelcomePage");
	}
}
