﻿using System.Collections.Generic;
using System.Linq;
using Shopify.Examples.Helpers;
using Shopify.Examples.LineItems;
using Shopify.Unity;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CartQuantityChangedEvent : UnityEvent<int> {
}

public class CheckoutManager : MonoBehaviour {
    public GameObject shippingoptionpanel;
    public GameObject minishippingoptionpanel;
    public GameObject menupanel;
	public Text totalPrice;

	private Cart _cart;

	public CartQuantityChangedEvent OnCartQuantityChanged = new CartQuantityChangedEvent();

	public CartItem CartItemTemplate;
	public RectTransform Content;

	private readonly Dictionary<string, ProductVariant> _idVariantMapping = new Dictionary<string, ProductVariant>();
	private readonly Dictionary<string, Product> _idProductMapping = new Dictionary<string, Product>();
	private readonly Dictionary<string, CartItem> _idCartPanelLineItemMapping = new Dictionary<string, CartItem>();
	private readonly List<CartItem> _lineItems = new List<CartItem>();

    // Use this for initialization
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }
    //* 
    public void shippingokSelected()
    {
        minishippingoptionpanel.SetActive(true);
        shippingoptionpanel.SetActive(false);
        shippingoptionpanel.GetComponent<EasyTween>().animationParts.ObjectState = UITween.AnimationParts.State.OPEN;
    }/**/
     //*  
    public void shippingcancelSelected()
    {
        minishippingoptionpanel.SetActive(true);
        shippingoptionpanel.SetActive(false);
        shippingoptionpanel.GetComponent<EasyTween>().animationParts.ObjectState = UITween.AnimationParts.State.OPEN;
    }/**/
    public void menubtnSelected()
    {
        menupanel.GetComponent<EasyTween>().OpenCloseObjectAnimation();
    }

	public void onHomeTabPressed()
	{
		Application.LoadLevel ("HomePage");
	}
	public void onAccountTabPressed()
	{
		Application.LoadLevel ("WelcomePage");
	}
	public void onDropDownPressed()
	{
        shippingoptionpanel.GetComponent<EasyTween>().OpenCloseObjectAnimation();

	}

	public void AddToCart(Product product, ProductVariant variant) {
		if (_cart == null) {
			_cart = ShopifyHelper.CreateCart();
		}

		print (product.title ());

		// Handle adding a particular variant to the cart
		// For more information on adding variants to the cart visit
		// https://help.shopify.com/api/sdks/custom-storefront/unity-buy-sdk/getting-started#create-cart-line-items-based-on-selected-options

		var existingLineItem = _cart.LineItems.Get(variant);
		if (existingLineItem == null) {
			_cart.LineItems.AddOrUpdate(variant);
			print ("new");

			var instance = Instantiate(CartItemTemplate);
			instance.transform.SetParent(Content, false);
			instance.SetCurrentProduct(product, variant, 1);

			instance.OnVariantLineItemQuantityAdjustment.AddListener(HandleVariantLineItemQuantityAdjustment);

			_idCartPanelLineItemMapping.Add(variant.id(), instance);
			_lineItems.Add(instance);
		} else {
			print ("exist");
			_cart.LineItems.AddOrUpdate(variant, existingLineItem.Quantity + 1);

			var cartPanelLineItem = _idCartPanelLineItemMapping[variant.id()];
			//cartPanelLineItem.gameObject.SetActive (false);
			cartPanelLineItem.Quantity.text = existingLineItem.Quantity.ToString();
		}

		if (!_idVariantMapping.ContainsKey(variant.id())) {
			_idVariantMapping.Add(variant.id(), variant);
		}

		if (!_idProductMapping.ContainsKey(variant.id())) {
			_idProductMapping.Add(variant.id(), product);
		}

		DispatchCartQuantityChanged();
	}

	// When the quantity adjustment buttons are clicked they will trigger this event handler.
	private void HandleVariantLineItemQuantityAdjustment(ProductVariant variant, int quantityAdjustment) {
		// First we find the lineitem associated with the the variant
		var lineItem = _cart.LineItems.Get(variant);
		// We then update the current quantity with the adjustment
		_cart.LineItems.AddOrUpdate(variant, lineItem.Quantity + quantityAdjustment);

		// In addition to updating the cart itself, we also need to update the associated UI elements. We begin
		// this process by retrieving the cart panel line item in question
		var cartPanelLineItem = _idCartPanelLineItemMapping[variant.id()];


		if (lineItem.Quantity < 1) {
			// If the lineitem quantity has reached zero, then we need to remove its visual representation
			// which is accomplished by destroying the game object
			Destroy(cartPanelLineItem.gameObject);
			// We also take care of removing the associated mappings
			_cart.LineItems.Delete(variant);
			_lineItems.Remove(cartPanelLineItem);
			_idCartPanelLineItemMapping.Remove(variant.id());
		} else {
			// In the case where we have a new, non zero quantity, we take care of updating the UI to 
			// reflect this changed quantity 
			print("handle asldkfjsldkfjlskdf");
			cartPanelLineItem.Quantity.text = lineItem.Quantity.ToString();
		}

		// Finally, we dispatch an event saying we've updated the cart quantity, so any secondary calculations 
		// can be handled
		DispatchCartQuantityChanged();
	}

	private void DispatchCartQuantityChanged() {
		var totalLineItemQuantity = 0;
		foreach (var lineItem in _cart.LineItems.All()) {
			totalLineItemQuantity += (int)lineItem.Quantity;
		}

		totalPrice.text = _cart.Subtotal().ToString("c");

		OnCartQuantityChanged.Invoke(totalLineItemQuantity);
	}
}
