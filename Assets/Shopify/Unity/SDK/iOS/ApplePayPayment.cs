#if UNITY_IOS
namespace Shopify.Unity.SDK.iOS {
    using System.Collections;
    using System.Collections.Generic;
    using Shopify.Unity;
    using Shopify.Unity.MiniJSON;

    public class ApplePayPayment {
        private enum PaymentField {
            BillingContact,
            ShippingContact,
            ShippingIdentifier,
            TokenData
        }

        private enum TokenField {
            PaymentData,
            TransactionIdentifier
        }

        readonly public MailingAddressInput BillingAddress;
        readonly public MailingAddressInput ShippingAddress;
        readonly public string Email;
        readonly public string ShippingIdentifier;
        readonly public string TransactionIdentifier;
        readonly public string PaymentData;

        public ApplePayPayment(string stringJson) {
            var dictionary = (Dictionary<string, object>)Json.Deserialize(stringJson);

            // Billing Address
            var billingContactDictionary = (Dictionary<string, object>)dictionary[PaymentField.BillingContact.ToString("G")];
            BillingAddress = new MailingAddressInput(billingContactDictionary);

            // Shipping Address
            var shippingContactDictionary = (Dictionary<string, object>)dictionary[PaymentField.ShippingContact.ToString("G")];
            ShippingAddress = new MailingAddressInput(shippingContactDictionary);

            // Email
            Email = (string)shippingContactDictionary["email"];

            // Shipping Identifier
            if (dictionary.ContainsKey(PaymentField.ShippingIdentifier.ToString("G"))) {
                ShippingIdentifier = (string)dictionary[PaymentField.ShippingIdentifier.ToString("G")];
            }

            // Transaction Identifier
            var tokenDictionary = (Dictionary<string, object>)dictionary[PaymentField.TokenData.ToString("G")];
            TransactionIdentifier = (string)tokenDictionary[TokenField.TransactionIdentifier.ToString("G")];

            // Payment Data
            // Payment Data can be null when running on a simulator
            if (tokenDictionary.ContainsKey(TokenField.PaymentData.ToString("G"))) {
                var paymentDictionary = tokenDictionary[TokenField.PaymentData.ToString("G")];
                PaymentData = Json.Serialize(paymentDictionary);
            } else {
                PaymentData = "";
            }
        }
    }
    }
    #endif
