#if UNITY_IOS
namespace Shopify.Unity.SDK.iOS {
    using System.Collections.Generic;
    using System.Collections;
    using System;
    [Serializable]
    public class ShippingMethod : SummaryItem {
        #pragma warning disable 0414
        public string Identifier;
        public string Detail;
        #pragma warning restore 0414

        public ShippingMethod(string label, string amount, string identifier, string detail = null) : base(label, amount) {
            Identifier = identifier;
            Detail = detail;
        }

        public override object ToJson() {
            var dict = (Dictionary<string, object>) base.ToJson();
            dict["Identifier"] = Identifier;
            dict["Detail"] = Detail;
            return dict;
        }
    }
    }
    #endif
