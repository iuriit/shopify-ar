#if UNITY_IOS
namespace Shopify.Unity.SDK.iOS {
    using System.Collections;
    using System.Collections.Generic;

    public interface IApplePayEventReceiver {
        void UpdateSummaryItemsForShippingIdentifier(string serializedMessage);
        void UpdateSummaryItemsForShippingContact(string serializedMessage);
        void FetchApplePayCheckoutStatusForToken(string serializedMessage);
        void DidFinishCheckoutSession(string serializedMessage);
    }
    }
    #endif
