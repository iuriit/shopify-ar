namespace Shopify.Unity.SDK {
    using System;
    using System.Collections.Generic;
    using Shopify.Unity.GraphQL;

    /// <summary>
    /// Generates default queries for <see ref="ShopifyClient.products">ShopifyClient.products </see> and <see ref="ShopifyClient.collections">ShopifyClient.collections </see>.
    /// </summary>
    public class DefaultQueries {
        public static readonly int MaxPageSize = 250;

        public static DefaultProductQueries products = new DefaultProductQueries();
        public static DefaultCollectionQueries collections = new DefaultCollectionQueries();
        public static DefaultCheckoutQueries checkout = new DefaultCheckoutQueries();
        public static DefaultShopQueries shop = new DefaultShopQueries();

        public static string GetAliasFromIndex(int idx) {
            return String.Format("a{0}", idx);
        }
    }
    }
