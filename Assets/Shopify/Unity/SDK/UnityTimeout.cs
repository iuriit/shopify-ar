#if !SHOPIFY_MONO_UNIT_TEST
namespace Shopify.Unity.SDK {
    using UnityEngine;
    using System;
    using System.Text;
    using System.Collections;

    internal class TimeoutComponent : MonoBehaviour {
        public void StartTimeout(float duration, Action callback) {
            StartCoroutine(DoTimeout(duration, callback));
        }

        private IEnumerator DoTimeout(float duration, Action callback) {
            yield return new WaitForSeconds(duration);

            callback();
        }
    }

    public class UnityTimeout {
        private static TimeoutComponent component;

        public static void Start(float duration, Action callback) {
            if (component == null) {
                component = GlobalGameObject.AddComponent<TimeoutComponent>();
            }

            component.StartTimeout(duration, callback);
        }
    }
    }
    #endif
