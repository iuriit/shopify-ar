namespace Shopify.Unity.SDK {
    using System.Collections.Generic;
    using System.Collections;
    using System;

    [Serializable]
    public class SummaryItem: Serializable {
        #pragma warning disable 0414
        public string Label;
        public string Amount;
        #pragma warning restore 0414

        public SummaryItem(string label, string amount) {
            Label = label;
            Amount = amount;
        }

        public override object ToJson() {
            var dict = new Dictionary<string, object>();
            dict["Label"] = Label;
            dict["Amount"] = Amount;
            return dict;
        }
    }
    }
