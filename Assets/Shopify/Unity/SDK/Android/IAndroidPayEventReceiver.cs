#if UNITY_ANDROID
namespace Shopify.Unity.SDK.Android {
    using System.Collections;
    using System.Collections.Generic;

    public interface IAndroidPayEventReceiver {
        void OnUpdateShippingAddress(string serializedMessage);
        void OnUpdateShippingLine(string serializedMessage);
        void OnError(string serializedMessage);
        void OnCancel(string serializedMessage);
    }
    }
    #endif
