#if UNITY_ANDROID
namespace Shopify.Unity.SDK.Android {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using Shopify.Unity.SDK;
    using Shopify.Unity.MiniJSON;

    #if !SHOPIFY_MONO_UNIT_TEST
    using UnityEngine;
    #endif

    public partial class AndroidNativeCheckout : INativeCheckout {
        private CartState CartState;
        private AndroidPayEventReceiverBridge AndroidPayEventBridge;

        private CheckoutSuccessCallback OnSuccess;
        private CheckoutCancelCallback OnCancelled;
        private CheckoutFailureCallback OnFailure;

        public AndroidNativeCheckout(CartState cartState) {
            CartState = cartState;
        }

        public bool CanShowPaymentSetup(PaymentSettings paymentSettings) {
            return false;
        }

        public void ShowPaymentSetup() {
            // TODO
        }

        public bool CanCheckout(PaymentSettings paymentSettings) {
            // TODO: Invoke methods in the plugin to determine if we can perform Android Pay
            return true;
        }

        public void Checkout(
            string key, 
            ShopMetadata shopMetadata,
            CheckoutSuccessCallback success, 
            CheckoutCancelCallback cancelled, 
            CheckoutFailureCallback failure
        ) {
            // TODO: Store callbacks and extract items we need from the cart to pass to Android Pay.
            OnSuccess = success;
            OnCancelled = cancelled;
            OnFailure = failure;

            var checkout = CartState.CurrentCheckout;

            // TODO: Replace with Shop name
            var merchantName = shopMetadata.Name;
            var pricingLineItems = GetPricingLineItemsFromCheckout(checkout);
            var pricingLineItemsString = Json.Serialize(pricingLineItems);
            var currencyCodeString = checkout.currencyCode().ToString("G");
            var countryCodeString = shopMetadata.PaymentSettings.countryCode().ToString("G");
            var requiresShipping = checkout.requiresShipping();

            #if !SHOPIFY_MONO_UNIT_TEST
            using (var androidPayCheckoutSession = new AndroidJavaObject("com.shopify.unity.buy.AndroidPayCheckoutSession")) {
                object[] args = { GlobalGameObject.Name, merchantName, key, pricingLineItemsString, currencyCodeString, countryCodeString, requiresShipping, true };
                if (androidPayCheckoutSession.Call<bool>("checkoutWithAndroidPay", args)) {
                    if (AndroidPayEventBridge == null) {
                        AndroidPayEventBridge = GlobalGameObject.AddComponent<AndroidPayEventReceiverBridge>();
                        AndroidPayEventBridge.Receiver = this;
                    }
                } else {
                    // TODO: Create more meaningful error?
                    var error = new ShopifyError(ShopifyError.ErrorType.NativePaymentProcessingError, "Unable to create Android Pay session.");
                    OnFailure(error);
                }
            }
            #endif
        }

        private Dictionary<string, string> GetPricingLineItemsFromCheckout(Checkout checkout) {
            var dict = new Dictionary<string, string>()
            {
                { "taxPrice",       checkout.totalTax().ToString() },
                { "subtotal",       checkout.subtotalPrice().ToString() },
                { "totalPrice",     checkout.totalPrice().ToString() }
            };

            if (checkout.requiresShipping()) {
                try {
                    dict.Add("shippingPrice", checkout.shippingLine().price().ToString());
                } catch {}
            }
            return dict;
        }
    }
    }
    #endif
