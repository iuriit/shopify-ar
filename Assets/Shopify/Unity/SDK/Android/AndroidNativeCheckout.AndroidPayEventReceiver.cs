#if UNITY_ANDROID
namespace Shopify.Unity.SDK.Android {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;
    using Shopify.Unity.MiniJSON;

    #if !SHOPIFY_MONO_UNIT_TEST
    using UnityEngine;
    #endif

    public partial class AndroidNativeCheckout : IAndroidPayEventReceiver {
        public void OnUpdateShippingAddress(string serializedMessage) {
        }

        public void OnUpdateShippingLine(string serializedMessage) {
        }

        public void OnError(string serializedMessage) {
        }

        public void OnCancel(string serializedMessage) {
        }
    }
    }
    #endif
