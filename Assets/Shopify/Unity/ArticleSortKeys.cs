namespace Shopify.Unity {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    /// <summary>
    /// The set of valid sort keys for the articles query.
    /// </summary>
    public enum ArticleSortKeys {
        /// <summary>
        /// If the SDK is not up to date with the schema in the Storefront API, it is possible
        /// to have enum values returned that are unknown to the SDK. In this case the value
        /// will actually be UNKNOWN.
        /// </summary>
        UNKNOWN,

        AUTHOR,

        BLOG_TITLE,

        ID,

        RELEVANCE,

        TITLE,

        UPDATED_AT
    }
    }
