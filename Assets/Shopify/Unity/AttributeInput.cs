namespace Shopify.Unity {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    /// <summary>
    /// Specifies the input fields required for an attribute.
    /// </summary>
    public class AttributeInput : InputBase {
        public const string keyFieldKey = "key";

        public const string valueFieldKey = "value";

        public string key {
            get {
                return (string) Get(keyFieldKey);
            }
            set {
                Set(keyFieldKey, value);
            }
        }

        public string value {
            get {
                return (string) Get(valueFieldKey);
            }
            set {
                Set(valueFieldKey, value);
            }
        }

        public AttributeInput(string key,string value) {
            Set(keyFieldKey, key);

            Set(valueFieldKey, value);
        }

        public AttributeInput(Dictionary<string, object> dataJSON) {
            try {
                Set(keyFieldKey, dataJSON[keyFieldKey]);

                Set(valueFieldKey, dataJSON[valueFieldKey]);
            } catch {
                throw;
            }
        }
    }
    }
