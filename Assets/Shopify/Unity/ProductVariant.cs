namespace Shopify.Unity {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Shopify.Unity.SDK;

    /// <summary>
    /// A product variant represents a different version of a product, such as differing sizes or differing colors.
    /// </summary>
    public class ProductVariant : AbstractResponse, ICloneable, Node {
        /// <summary>
        /// <see ref="ProductVariant" /> Accepts deserialized json data.
        /// <see ref="ProductVariant" /> Will further parse passed in data.
        /// </summary>
        /// <param name="dataJSON">Deserialized JSON data for ProductVariant</param>
        public ProductVariant(Dictionary<string, object> dataJSON) {
            DataJSON = dataJSON;
            Data = new Dictionary<string,object>();

            foreach (string key in dataJSON.Keys) {
                string fieldName = key;
                Regex regexAlias = new Regex("^(.+)___.+$");
                Match match = regexAlias.Match(key);

                if (match.Success) {
                    fieldName = match.Groups[1].Value;
                }

                switch(fieldName) {
                    case "available":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            (bool) dataJSON[key]
                        );
                    }

                    break;

                    case "availableForSale":

                    Data.Add(
                        key,

                        (bool) dataJSON[key]
                    );

                    break;

                    case "compareAtPrice":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            Convert.ToDecimal(dataJSON[key])
                        );
                    }

                    break;

                    case "id":

                    Data.Add(
                        key,

                        (string) dataJSON[key]
                    );

                    break;

                    case "image":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            new Image((Dictionary<string,object>) dataJSON[key])
                        );
                    }

                    break;

                    case "price":

                    Data.Add(
                        key,

                        Convert.ToDecimal(dataJSON[key])
                    );

                    break;

                    case "product":

                    Data.Add(
                        key,

                        new Product((Dictionary<string,object>) dataJSON[key])
                    );

                    break;

                    case "selectedOptions":

                    Data.Add(
                        key,

                        CastUtils.CastList<List<SelectedOption>>((IList) dataJSON[key])
                    );

                    break;

                    case "sku":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            (string) dataJSON[key]
                        );
                    }

                    break;

                    case "title":

                    Data.Add(
                        key,

                        (string) dataJSON[key]
                    );

                    break;

                    case "weight":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            (double) dataJSON[key]
                        );
                    }

                    break;

                    case "weightUnit":

                    Data.Add(
                        key,

                        CastUtils.GetEnumValue<WeightUnit>(dataJSON[key])
                    );

                    break;
                }
            }
        }

        /// \deprecated Use `availableForSale` instead
        /// <summary>
        /// Indicates if the product variant is in stock.
        /// </summary>
        public bool? available() {
            return Get<bool?>("available");
        }

        /// <summary>
        /// Indicates if the product variant is available for sale.
        /// </summary>
        public bool availableForSale() {
            return Get<bool>("availableForSale");
        }

        /// <summary>
        /// The compare at price of the variant. This can be used to mark a variant as on sale, when `compareAtPrice` is higher than `price`.
        /// </summary>
        public decimal compareAtPrice() {
            return Get<decimal>("compareAtPrice");
        }

        /// <summary>
        /// Globally unique identifier.
        /// </summary>
        public string id() {
            return Get<string>("id");
        }

        /// <summary>
        /// Image associated with the product variant.
        /// </summary>
        /// <param name="alias">
        /// If the original field queried was queried using an alias, then pass the matching string.
        /// </param>
        public Image image(string alias = null) {
            return Get<Image>("image", alias);
        }

        /// <summary>
        /// The product variant’s price.
        /// </summary>
        public decimal price() {
            return Get<decimal>("price");
        }

        /// <summary>
        /// The product object that the product variant belongs to.
        /// </summary>
        public Product product() {
            return Get<Product>("product");
        }

        /// <summary>
        /// List of product options applied to the variant.
        /// </summary>
        public List<SelectedOption> selectedOptions() {
            return Get<List<SelectedOption>>("selectedOptions");
        }

        /// <summary>
        /// The SKU (Stock Keeping Unit) associated with the variant.
        /// </summary>
        public string sku() {
            return Get<string>("sku");
        }

        /// <summary>
        /// The product variant’s title.
        /// </summary>
        public string title() {
            return Get<string>("title");
        }

        /// <summary>
        /// The weight of the product variant in the unit system specified with `weight_unit`.
        /// </summary>
        public double? weight() {
            return Get<double?>("weight");
        }

        /// <summary>
        /// Unit of measurement for weight.
        /// </summary>
        public WeightUnit weightUnit() {
            return Get<WeightUnit>("weightUnit");
        }

        public object Clone() {
            return new ProductVariant(DataJSON);
        }

        private static List<Node> DataToNodeList(object data) {
            var objects = (List<object>)data;
            var nodes = new List<Node>();

            foreach (var obj in objects) {
                nodes.Add(UnknownNode.Create((Dictionary<string,object>) obj));
            }

            return nodes;
        }
    }
    }
