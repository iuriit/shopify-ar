namespace Shopify.Unity {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Shopify.Unity.SDK;

    /// <summary>
    /// The schema’s entry-point for queries. This acts as the public, top-level API from which all queries must start.
    /// </summary>
    public class QueryRoot : AbstractResponse, ICloneable {
        /// <summary>
        /// <see ref="QueryRoot" /> Accepts deserialized json data.
        /// <see ref="QueryRoot" /> Will further parse passed in data.
        /// </summary>
        /// <param name="dataJSON">Deserialized JSON data for QueryRoot</param>
        public QueryRoot(Dictionary<string, object> dataJSON) {
            DataJSON = dataJSON;
            Data = new Dictionary<string,object>();

            foreach (string key in dataJSON.Keys) {
                string fieldName = key;
                Regex regexAlias = new Regex("^(.+)___.+$");
                Match match = regexAlias.Match(key);

                if (match.Success) {
                    fieldName = match.Groups[1].Value;
                }

                switch(fieldName) {
                    case "customer":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            new Customer((Dictionary<string,object>) dataJSON[key])
                        );
                    }

                    break;

                    case "node":

                    if (dataJSON[key] == null) {
                        Data.Add(key, null);
                    } else {
                        Data.Add(
                            key,

                            UnknownNode.Create((Dictionary<string,object>) dataJSON[key])
                        );
                    }

                    break;

                    case "nodes":

                    Data.Add(
                        key,

                        DataToNodeList(dataJSON[key])
                    );

                    break;

                    case "shop":

                    Data.Add(
                        key,

                        new Shop((Dictionary<string,object>) dataJSON[key])
                    );

                    break;
                }
            }
        }

        /// <param name="alias">
        /// If the original field queried was queried using an alias, then pass the matching string.
        /// </param>
        public Customer customer(string alias = null) {
            return Get<Customer>("customer", alias);
        }

        /// <param name="alias">
        /// If the original field queried was queried using an alias, then pass the matching string.
        /// </param>
        public Node node(string alias = null) {
            return Get<Node>("node", alias);
        }

        /// <param name="alias">
        /// If the original field queried was queried using an alias, then pass the matching string.
        /// </param>
        public List<Node> nodes(string alias = null) {
            return Get<List<Node>>("nodes", alias);
        }

        public Shop shop() {
            return Get<Shop>("shop");
        }

        public object Clone() {
            return new QueryRoot(DataJSON);
        }

        private static List<Node> DataToNodeList(object data) {
            var objects = (List<object>)data;
            var nodes = new List<Node>();

            foreach (var obj in objects) {
                nodes.Add(UnknownNode.Create((Dictionary<string,object>) obj));
            }

            return nodes;
        }
    }
    }
