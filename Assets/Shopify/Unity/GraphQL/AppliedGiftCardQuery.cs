namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void AppliedGiftCardDelegate(AppliedGiftCardQuery query);

    /// <summary>
    /// Details about the gift card used on the checkout.
    /// </summary>
    public class AppliedGiftCardQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="AppliedGiftCardQuery" /> is used to build queries. Typically
        /// <see cref="AppliedGiftCardQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public AppliedGiftCardQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// The amount that was used taken from the Gift Card by applying it.
        /// </summary>
        public AppliedGiftCardQuery amountUsed() {
            Query.Append("amountUsed ");

            return this;
        }

        /// <summary>
        /// The amount left on the Gift Card.
        /// </summary>
        public AppliedGiftCardQuery balance() {
            Query.Append("balance ");

            return this;
        }

        /// <summary>
        /// Globally unique identifier.
        /// </summary>
        public AppliedGiftCardQuery id() {
            Query.Append("id ");

            return this;
        }

        /// <summary>
        /// The last characters of the Gift Card code
        /// </summary>
        public AppliedGiftCardQuery lastCharacters() {
            Query.Append("lastCharacters ");

            return this;
        }
    }
    }
