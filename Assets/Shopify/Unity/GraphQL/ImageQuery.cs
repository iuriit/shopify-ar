namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void ImageDelegate(ImageQuery query);

    /// <summary>
    /// Represents an image resource.
    /// </summary>
    public class ImageQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="ImageQuery" /> is used to build queries. Typically
        /// <see cref="ImageQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public ImageQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// A word or phrase to share the nature or contents of an image.
        /// </summary>
        public ImageQuery altText() {
            Query.Append("altText ");

            return this;
        }

        /// <summary>
        /// A unique identifier for the image.
        /// </summary>
        public ImageQuery id() {
            Query.Append("id ");

            return this;
        }

        /// <summary>
        /// The location of the image as a URL.
        /// </summary>
        public ImageQuery src() {
            Query.Append("src ");

            return this;
        }
    }
    }
