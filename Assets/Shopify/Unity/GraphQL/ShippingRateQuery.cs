namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void ShippingRateDelegate(ShippingRateQuery query);

    /// <summary>
    /// A shipping rate to be applied to a checkout.
    /// </summary>
    public class ShippingRateQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="ShippingRateQuery" /> is used to build queries. Typically
        /// <see cref="ShippingRateQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public ShippingRateQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// Human-readable unique identifier for this shipping rate.
        /// </summary>
        public ShippingRateQuery handle() {
            Query.Append("handle ");

            return this;
        }

        /// <summary>
        /// Price of this shipping rate.
        /// </summary>
        public ShippingRateQuery price() {
            Query.Append("price ");

            return this;
        }

        /// <summary>
        /// Title of this shipping rate.
        /// </summary>
        public ShippingRateQuery title() {
            Query.Append("title ");

            return this;
        }
    }
    }
