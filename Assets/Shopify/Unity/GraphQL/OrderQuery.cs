namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void OrderDelegate(OrderQuery query);

    /// <summary>
    /// An order is a customer’s completed request to purchase one or more products from a shop. An order is created when a customer completes the checkout process, during which time they provides an email address, billing address and payment information.
    /// </summary>
    public class OrderQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="OrderQuery" /> is used to build queries. Typically
        /// <see cref="OrderQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public OrderQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// The code of the currency used for the payment.
        /// </summary>
        public OrderQuery currencyCode() {
            Query.Append("currencyCode ");

            return this;
        }

        /// <summary>
        /// The locale code in which this specific order happened.
        /// </summary>
        public OrderQuery customerLocale() {
            Query.Append("customerLocale ");

            return this;
        }

        /// <summary>
        /// The order’s URL for a customer.
        /// </summary>
        public OrderQuery customerUrl() {
            Query.Append("customerUrl ");

            return this;
        }

        /// <summary>
        /// The customer's email address.
        /// </summary>
        public OrderQuery email() {
            Query.Append("email ");

            return this;
        }

        /// <summary>
        /// Globally unique identifier.
        /// </summary>
        public OrderQuery id() {
            Query.Append("id ");

            return this;
        }

        /// <summary>
        /// List of the order’s line items.
        /// </summary>
        public OrderQuery lineItems(OrderLineItemConnectionDelegate buildQuery,long? first = null,string after = null,long? last = null,string before = null,bool? reverse = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("lineItems___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("lineItems ");

            Arguments args = new Arguments();

            if (first != null) {
                args.Add("first", first);
            }

            if (after != null) {
                args.Add("after", after);
            }

            if (last != null) {
                args.Add("last", last);
            }

            if (before != null) {
                args.Add("before", before);
            }

            if (reverse != null) {
                args.Add("reverse", reverse);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new OrderLineItemConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// A unique numeric identifier for the order for use by shop owner and customer.
        /// </summary>
        public OrderQuery orderNumber() {
            Query.Append("orderNumber ");

            return this;
        }

        /// <summary>
        /// The customer's phone number.
        /// </summary>
        public OrderQuery phone() {
            Query.Append("phone ");

            return this;
        }

        /// <summary>
        /// The date and time when the order was imported.
        /// This value can be set to dates in the past when importing from other systems.
        /// If no value is provided, it will be auto-generated based on current date and time.
        /// </summary>
        public OrderQuery processedAt() {
            Query.Append("processedAt ");

            return this;
        }

        /// <summary>
        /// The address to where the order will be shipped.
        /// </summary>
        public OrderQuery shippingAddress(MailingAddressDelegate buildQuery) {
            Query.Append("shippingAddress ");

            Query.Append("{");
            buildQuery(new MailingAddressQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Price of the order before shipping and taxes.
        /// </summary>
        public OrderQuery subtotalPrice() {
            Query.Append("subtotalPrice ");

            return this;
        }

        /// <summary>
        /// The sum of all the prices of all the items in the order, taxes and discounts included (must be positive).
        /// </summary>
        public OrderQuery totalPrice() {
            Query.Append("totalPrice ");

            return this;
        }

        /// <summary>
        /// The total amount that has been refunded.
        /// </summary>
        public OrderQuery totalRefunded() {
            Query.Append("totalRefunded ");

            return this;
        }

        /// <summary>
        /// The total cost of shipping.
        /// </summary>
        public OrderQuery totalShippingPrice() {
            Query.Append("totalShippingPrice ");

            return this;
        }

        /// <summary>
        /// The total cost of taxes.
        /// </summary>
        public OrderQuery totalTax() {
            Query.Append("totalTax ");

            return this;
        }
    }
    }
