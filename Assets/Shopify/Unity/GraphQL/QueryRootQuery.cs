namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void QueryRootDelegate(QueryRootQuery query);

    /// <summary>
    /// <see cref="QueryRootQuery" /> is the root query builder. All Storefront API queries are built off of <see cref="QueryRootQuery" />.
    /// </summary>
    public class QueryRootQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="QueryRootQuery" /> constructor accepts no parameters but it will create a root
        /// query builder.
        /// </summary>
        public QueryRootQuery() {
            Query = new StringBuilder("{");
        }

        /// <param name="customerAccessToken">
        /// The customer access token
        /// </param>
        public QueryRootQuery customer(CustomerDelegate buildQuery,string customerAccessToken,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customer___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customer ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerQuery(Query));
            Query.Append("}");

            return this;
        }

        public QueryRootQuery node(NodeDelegate buildQuery,string id,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("node___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("node ");

            Arguments args = new Arguments();

            args.Add("id", id);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new NodeQuery(Query));
            Query.Append("}");

            return this;
        }

        public QueryRootQuery nodes(NodeDelegate buildQuery,List<string> ids,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("nodes___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("nodes ");

            Arguments args = new Arguments();

            args.Add("ids", ids);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new NodeQuery(Query));
            Query.Append("}");

            return this;
        }

        public QueryRootQuery shop(ShopDelegate buildQuery) {
            Query.Append("shop ");

            Query.Append("{");
            buildQuery(new ShopQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Will return a GraphQL query.
        /// </summary>
        public override string ToString() {
            return Query.ToString() + "}";
        }
    }
    }
