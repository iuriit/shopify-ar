namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void CustomerActivatePayloadDelegate(CustomerActivatePayloadQuery query);

    public class CustomerActivatePayloadQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="CustomerActivatePayloadQuery" /> is used to build queries. Typically
        /// <see cref="CustomerActivatePayloadQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public CustomerActivatePayloadQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// The customer object.
        /// </summary>
        public CustomerActivatePayloadQuery customer(CustomerDelegate buildQuery) {
            Query.Append("customer ");

            Query.Append("{");
            buildQuery(new CustomerQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of errors that occurred executing the mutation.
        /// </summary>
        public CustomerActivatePayloadQuery userErrors(UserErrorDelegate buildQuery) {
            Query.Append("userErrors ");

            Query.Append("{");
            buildQuery(new UserErrorQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
