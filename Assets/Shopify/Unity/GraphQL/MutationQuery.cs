namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void MutationDelegate(MutationQuery query);

    /// <summary>
    /// <see cref="MutationQuery" /> is the root mutation builder. All Storefront API mutation queries are built off of <see cref="MutationQuery" />.
    /// </summary>
    public class MutationQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="MutationQuery" /> constructor accepts no parameters but it will create a root
        /// mutation builder.
        /// </summary>
        public MutationQuery() {
            Query = new StringBuilder("mutation{");
        }

        /// <summary>
        /// Updates the attributes of a checkout.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutAttributesUpdate(CheckoutAttributesUpdatePayloadDelegate buildQuery,string checkoutId,CheckoutAttributesUpdateInput input,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutAttributesUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutAttributesUpdate ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("input", input);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutAttributesUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutCompleteFree(CheckoutCompleteFreePayloadDelegate buildQuery,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutCompleteFree___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutCompleteFree ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutCompleteFreePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Completes a checkout using a credit card token from Shopify's Vault.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutCompleteWithCreditCard(CheckoutCompleteWithCreditCardPayloadDelegate buildQuery,string checkoutId,CreditCardPaymentInput payment,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutCompleteWithCreditCard___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutCompleteWithCreditCard ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("payment", payment);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutCompleteWithCreditCardPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Completes a checkout with a tokenized payment.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutCompleteWithTokenizedPayment(CheckoutCompleteWithTokenizedPaymentPayloadDelegate buildQuery,string checkoutId,TokenizedPaymentInput payment,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutCompleteWithTokenizedPayment___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutCompleteWithTokenizedPayment ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("payment", payment);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutCompleteWithTokenizedPaymentPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Creates a new checkout.
        /// </summary>
        public MutationQuery checkoutCreate(CheckoutCreatePayloadDelegate buildQuery,CheckoutCreateInput input,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutCreate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutCreate ");

            Arguments args = new Arguments();

            args.Add("input", input);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutCreatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Associates a customer to the checkout.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        /// <param name="customerAccessToken">
        /// The customer access token of the customer to associate.
        /// </param>
        public MutationQuery checkoutCustomerAssociate(CheckoutCustomerAssociatePayloadDelegate buildQuery,string checkoutId,string customerAccessToken,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutCustomerAssociate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutCustomerAssociate ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("customerAccessToken", customerAccessToken);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutCustomerAssociatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Disassociates the current checkout customer from the checkout.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutCustomerDisassociate(CheckoutCustomerDisassociatePayloadDelegate buildQuery,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutCustomerDisassociate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutCustomerDisassociate ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutCustomerDisassociatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Applies a discount to an existing checkout using a discount code.
        /// </summary>
        /// <param name="discountCode">
        /// The discount code to apply to the checkout.
        /// </param>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutDiscountCodeApply(CheckoutDiscountCodeApplyPayloadDelegate buildQuery,string discountCode,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutDiscountCodeApply___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutDiscountCodeApply ");

            Arguments args = new Arguments();

            args.Add("discountCode", discountCode);

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutDiscountCodeApplyPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates the email on an existing checkout.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        /// <param name="email">
        /// The email to update the checkout with.
        /// </param>
        public MutationQuery checkoutEmailUpdate(CheckoutEmailUpdatePayloadDelegate buildQuery,string checkoutId,string email,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutEmailUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutEmailUpdate ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("email", email);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutEmailUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Applies a gift card to an existing checkout using a gift card code.
        /// </summary>
        /// <param name="giftCardCode">
        /// The code of the gift card to apply on the checkout.
        /// </param>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutGiftCardApply(CheckoutGiftCardApplyPayloadDelegate buildQuery,string giftCardCode,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutGiftCardApply___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutGiftCardApply ");

            Arguments args = new Arguments();

            args.Add("giftCardCode", giftCardCode);

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutGiftCardApplyPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Removes an applied gift card from the checkout.
        /// </summary>
        /// <param name="appliedGiftCardId">
        /// The ID of the Applied Gift Card to remove from the Checkout
        /// </param>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutGiftCardRemove(CheckoutGiftCardRemovePayloadDelegate buildQuery,string appliedGiftCardId,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutGiftCardRemove___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutGiftCardRemove ");

            Arguments args = new Arguments();

            args.Add("appliedGiftCardId", appliedGiftCardId);

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutGiftCardRemovePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Adds a list of line items to a checkout.
        /// </summary>
        /// <param name="lineItems">
        /// A list of line item objects to add to the checkout.
        /// </param>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutLineItemsAdd(CheckoutLineItemsAddPayloadDelegate buildQuery,List<CheckoutLineItemInput> lineItems,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutLineItemsAdd___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutLineItemsAdd ");

            Arguments args = new Arguments();

            args.Add("lineItems", lineItems);

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutLineItemsAddPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Removes line items from an existing checkout
        /// </summary>
        /// <param name="checkoutId">
        /// the checkout on which to remove line items
        /// </param>
        /// <param name="lineItemIds">
        /// line item ids to remove
        /// </param>
        public MutationQuery checkoutLineItemsRemove(CheckoutLineItemsRemovePayloadDelegate buildQuery,string checkoutId,List<string> lineItemIds,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutLineItemsRemove___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutLineItemsRemove ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("lineItemIds", lineItemIds);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutLineItemsRemovePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates line items on a checkout.
        /// </summary>
        /// <param name="checkoutId">
        /// the checkout on which to update line items.
        /// </param>
        /// <param name="lineItems">
        /// line items to update.
        /// </param>
        public MutationQuery checkoutLineItemsUpdate(CheckoutLineItemsUpdatePayloadDelegate buildQuery,string checkoutId,List<CheckoutLineItemUpdateInput> lineItems,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutLineItemsUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutLineItemsUpdate ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("lineItems", lineItems);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutLineItemsUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates the shipping address of an existing checkout.
        /// </summary>
        /// <param name="shippingAddress">
        /// The shipping address to where the line items will be shipped.
        /// </param>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        public MutationQuery checkoutShippingAddressUpdate(CheckoutShippingAddressUpdatePayloadDelegate buildQuery,MailingAddressInput shippingAddress,string checkoutId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutShippingAddressUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutShippingAddressUpdate ");

            Arguments args = new Arguments();

            args.Add("shippingAddress", shippingAddress);

            args.Add("checkoutId", checkoutId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutShippingAddressUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates the shipping lines on an existing checkout.
        /// </summary>
        /// <param name="checkoutId">
        /// The ID of the checkout.
        /// </param>
        /// <param name="shippingRateHandle">
        /// A concatenation of a Checkout’s shipping provider, price, and title, enabling the customer to select the availableShippingRates.
        /// </param>
        public MutationQuery checkoutShippingLineUpdate(CheckoutShippingLineUpdatePayloadDelegate buildQuery,string checkoutId,string shippingRateHandle,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("checkoutShippingLineUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("checkoutShippingLineUpdate ");

            Arguments args = new Arguments();

            args.Add("checkoutId", checkoutId);

            args.Add("shippingRateHandle", shippingRateHandle);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CheckoutShippingLineUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Creates a customer access token.
        /// The customer access token is required to modify the customer object in any way.
        /// </summary>
        public MutationQuery customerAccessTokenCreate(CustomerAccessTokenCreatePayloadDelegate buildQuery,CustomerAccessTokenCreateInput input,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerAccessTokenCreate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerAccessTokenCreate ");

            Arguments args = new Arguments();

            args.Add("input", input);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerAccessTokenCreatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Permanently destroys a customer access token.
        /// </summary>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        public MutationQuery customerAccessTokenDelete(CustomerAccessTokenDeletePayloadDelegate buildQuery,string customerAccessToken,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerAccessTokenDelete___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerAccessTokenDelete ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerAccessTokenDeletePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Renews a customer access token.
        /// </summary>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        public MutationQuery customerAccessTokenRenew(CustomerAccessTokenRenewPayloadDelegate buildQuery,string customerAccessToken,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerAccessTokenRenew___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerAccessTokenRenew ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerAccessTokenRenewPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Activates a customer.
        /// </summary>
        /// <param name="id">
        /// Specifies the customer to activate.
        /// </param>
        public MutationQuery customerActivate(CustomerActivatePayloadDelegate buildQuery,string id,CustomerActivateInput input,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerActivate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerActivate ");

            Arguments args = new Arguments();

            args.Add("id", id);

            args.Add("input", input);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerActivatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Creates a new address for a customer.
        /// </summary>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        /// <param name="address">
        /// The customer mailing address to create.
        /// </param>
        public MutationQuery customerAddressCreate(CustomerAddressCreatePayloadDelegate buildQuery,string customerAccessToken,MailingAddressInput address,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerAddressCreate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerAddressCreate ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            args.Add("address", address);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerAddressCreatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Permanently deletes the address of an existing customer.
        /// </summary>
        /// <param name="id">
        /// Specifies the address to delete.
        /// </param>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        public MutationQuery customerAddressDelete(CustomerAddressDeletePayloadDelegate buildQuery,string id,string customerAccessToken,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerAddressDelete___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerAddressDelete ");

            Arguments args = new Arguments();

            args.Add("id", id);

            args.Add("customerAccessToken", customerAccessToken);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerAddressDeletePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates the address of an existing customer.
        /// </summary>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        /// <param name="id">
        /// Specifies the customer address to update.
        /// </param>
        /// <param name="address">
        /// The customer’s mailing address.
        /// </param>
        public MutationQuery customerAddressUpdate(CustomerAddressUpdatePayloadDelegate buildQuery,string customerAccessToken,string id,MailingAddressInput address,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerAddressUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerAddressUpdate ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            args.Add("id", id);

            args.Add("address", address);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerAddressUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        public MutationQuery customerCreate(CustomerCreatePayloadDelegate buildQuery,CustomerCreateInput input,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerCreate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerCreate ");

            Arguments args = new Arguments();

            args.Add("input", input);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerCreatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates the default address of an existing customer.
        /// </summary>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        /// <param name="addressId">
        /// ID of the address to set as the new default for the customer.
        /// </param>
        public MutationQuery customerDefaultAddressUpdate(CustomerDefaultAddressUpdatePayloadDelegate buildQuery,string customerAccessToken,string addressId,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerDefaultAddressUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerDefaultAddressUpdate ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            args.Add("addressId", addressId);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerDefaultAddressUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Sends a reset password email to the customer, as the first step in the reset password process.
        /// </summary>
        /// <param name="email">
        /// The email address of the customer to recover.
        /// </param>
        public MutationQuery customerRecover(CustomerRecoverPayloadDelegate buildQuery,string email,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerRecover___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerRecover ");

            Arguments args = new Arguments();

            args.Add("email", email);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerRecoverPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Resets a customer’s password with a token received from `CustomerRecover`.
        /// </summary>
        /// <param name="id">
        /// Specifies the customer to reset.
        /// </param>
        public MutationQuery customerReset(CustomerResetPayloadDelegate buildQuery,string id,CustomerResetInput input,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerReset___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerReset ");

            Arguments args = new Arguments();

            args.Add("id", id);

            args.Add("input", input);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerResetPayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Updates an existing customer.
        /// </summary>
        /// <param name="customerAccessToken">
        /// The access token used to identify the customer.
        /// </param>
        /// <param name="customer">
        /// The customer object input.
        /// </param>
        public MutationQuery customerUpdate(CustomerUpdatePayloadDelegate buildQuery,string customerAccessToken,CustomerUpdateInput customer,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("customerUpdate___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("customerUpdate ");

            Arguments args = new Arguments();

            args.Add("customerAccessToken", customerAccessToken);

            args.Add("customer", customer);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CustomerUpdatePayloadQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Will return a GraphQL query.
        /// </summary>
        public override string ToString() {
            return Query.ToString() + "}";
        }
    }
    }
