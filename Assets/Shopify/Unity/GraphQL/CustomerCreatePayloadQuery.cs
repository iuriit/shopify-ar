namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void CustomerCreatePayloadDelegate(CustomerCreatePayloadQuery query);

    public class CustomerCreatePayloadQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="CustomerCreatePayloadQuery" /> is used to build queries. Typically
        /// <see cref="CustomerCreatePayloadQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public CustomerCreatePayloadQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// The created customer object.
        /// </summary>
        public CustomerCreatePayloadQuery customer(CustomerDelegate buildQuery) {
            Query.Append("customer ");

            Query.Append("{");
            buildQuery(new CustomerQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of errors that occurred executing the mutation.
        /// </summary>
        public CustomerCreatePayloadQuery userErrors(UserErrorDelegate buildQuery) {
            Query.Append("userErrors ");

            Query.Append("{");
            buildQuery(new UserErrorQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
