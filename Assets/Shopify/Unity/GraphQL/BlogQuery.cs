namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void BlogDelegate(BlogQuery query);

    public class BlogQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="BlogQuery" /> is used to build queries. Typically
        /// <see cref="BlogQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public BlogQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// List of the blog's articles.
        /// </summary>
        public BlogQuery articles(ArticleConnectionDelegate buildQuery,long? first = null,string after = null,long? last = null,string before = null,bool? reverse = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("articles___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("articles ");

            Arguments args = new Arguments();

            if (first != null) {
                args.Add("first", first);
            }

            if (after != null) {
                args.Add("after", after);
            }

            if (last != null) {
                args.Add("last", last);
            }

            if (before != null) {
                args.Add("before", before);
            }

            if (reverse != null) {
                args.Add("reverse", reverse);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new ArticleConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Globally unique identifier.
        /// </summary>
        public BlogQuery id() {
            Query.Append("id ");

            return this;
        }

        /// <summary>
        /// The blogs’s title.
        /// </summary>
        public BlogQuery title() {
            Query.Append("title ");

            return this;
        }

        /// <summary>
        /// The url pointing to the blog accessible from the web.
        /// </summary>
        public BlogQuery url() {
            Query.Append("url ");

            return this;
        }
    }
    }
