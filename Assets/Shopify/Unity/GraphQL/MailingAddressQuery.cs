namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void MailingAddressDelegate(MailingAddressQuery query);

    /// <summary>
    /// Represents a mailing address for customers and shipping.
    /// </summary>
    public class MailingAddressQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="MailingAddressQuery" /> is used to build queries. Typically
        /// <see cref="MailingAddressQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public MailingAddressQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// Address line 1 (Street address/PO Box/Company name).
        /// </summary>
        public MailingAddressQuery address1() {
            Query.Append("address1 ");

            return this;
        }

        /// <summary>
        /// Address line 2 (Apartment/Suite/Unit/Building).
        /// </summary>
        public MailingAddressQuery address2() {
            Query.Append("address2 ");

            return this;
        }

        /// <summary>
        /// City/District/Suburb/Town/Village.
        /// </summary>
        public MailingAddressQuery city() {
            Query.Append("city ");

            return this;
        }

        /// <summary>
        /// Company/Organization/Government.
        /// </summary>
        public MailingAddressQuery company() {
            Query.Append("company ");

            return this;
        }

        /// <summary>
        /// State/County/Province/Region.
        /// </summary>
        public MailingAddressQuery country() {
            Query.Append("country ");

            return this;
        }

        /// <summary>
        /// Two-letter country code.
        /// 
        /// For example, US.
        /// </summary>
        public MailingAddressQuery countryCode() {
            Query.Append("countryCode ");

            return this;
        }

        /// <summary>
        /// First name of the customer.
        /// </summary>
        public MailingAddressQuery firstName() {
            Query.Append("firstName ");

            return this;
        }

        public MailingAddressQuery formatted(bool? withName = null,bool? withCompany = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("formatted___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("formatted ");

            Arguments args = new Arguments();

            if (withName != null) {
                args.Add("withName", withName);
            }

            if (withCompany != null) {
                args.Add("withCompany", withCompany);
            }

            Query.Append(args.ToString());

            return this;
        }

        /// <summary>
        /// Comma-separated list of city, province, and country.
        /// </summary>
        public MailingAddressQuery formattedArea() {
            Query.Append("formattedArea ");

            return this;
        }

        /// <summary>
        /// Globally unique identifier.
        /// </summary>
        public MailingAddressQuery id() {
            Query.Append("id ");

            return this;
        }

        /// <summary>
        /// Last name of the customer.
        /// </summary>
        public MailingAddressQuery lastName() {
            Query.Append("lastName ");

            return this;
        }

        /// <summary>
        /// Latitude coordinate of the customer address.
        /// </summary>
        public MailingAddressQuery latitude() {
            Query.Append("latitude ");

            return this;
        }

        /// <summary>
        /// Longitude coordinate of the customer address.
        /// </summary>
        public MailingAddressQuery longitude() {
            Query.Append("longitude ");

            return this;
        }

        /// <summary>
        /// Name of the customer, based on first name + last name.
        /// </summary>
        public MailingAddressQuery name() {
            Query.Append("name ");

            return this;
        }

        /// <summary>
        /// Unique phone number for the customer.
        /// 
        /// Formatted using E.164 standard. For example, _+16135551111_.
        /// </summary>
        public MailingAddressQuery phone() {
            Query.Append("phone ");

            return this;
        }

        /// <summary>
        /// State/County/Province/Region.
        /// </summary>
        public MailingAddressQuery province() {
            Query.Append("province ");

            return this;
        }

        /// <summary>
        /// Two-letter province or state code.
        /// 
        /// For example, ON.
        /// </summary>
        public MailingAddressQuery provinceCode() {
            Query.Append("provinceCode ");

            return this;
        }

        /// <summary>
        /// Zip/Postal Code.
        /// </summary>
        public MailingAddressQuery zip() {
            Query.Append("zip ");

            return this;
        }
    }
    }
