namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void CheckoutLineItemsRemovePayloadDelegate(CheckoutLineItemsRemovePayloadQuery query);

    public class CheckoutLineItemsRemovePayloadQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="CheckoutLineItemsRemovePayloadQuery" /> is used to build queries. Typically
        /// <see cref="CheckoutLineItemsRemovePayloadQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public CheckoutLineItemsRemovePayloadQuery(StringBuilder query) {
            Query = query;
        }

        public CheckoutLineItemsRemovePayloadQuery checkout(CheckoutDelegate buildQuery) {
            Query.Append("checkout ");

            Query.Append("{");
            buildQuery(new CheckoutQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of errors that occurred executing the mutation.
        /// </summary>
        public CheckoutLineItemsRemovePayloadQuery userErrors(UserErrorDelegate buildQuery) {
            Query.Append("userErrors ");

            Query.Append("{");
            buildQuery(new UserErrorQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
