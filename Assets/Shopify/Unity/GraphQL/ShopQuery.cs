namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void ShopDelegate(ShopQuery query);

    /// <summary>
    /// Shop represents a collection of the general settings and information about the shop.
    /// </summary>
    public class ShopQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="ShopQuery" /> is used to build queries. Typically
        /// <see cref="ShopQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public ShopQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// List of the shop' articles.
        /// </summary>
        /// <param name="query">
        /// Supported filter parameters:
        /// - `author`
        /// - `updated_at`
        /// - `created_at`
        /// - `blog_title`
        /// - `tag`
        /// </param>
        public ShopQuery articles(ArticleConnectionDelegate buildQuery,long? first = null,string after = null,long? last = null,string before = null,bool? reverse = null,ArticleSortKeys? sortKey = null,string queryValue = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("articles___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("articles ");

            Arguments args = new Arguments();

            if (first != null) {
                args.Add("first", first);
            }

            if (after != null) {
                args.Add("after", after);
            }

            if (last != null) {
                args.Add("last", last);
            }

            if (before != null) {
                args.Add("before", before);
            }

            if (reverse != null) {
                args.Add("reverse", reverse);
            }

            if (sortKey != null) {
                args.Add("sortKey", sortKey);
            }

            if (queryValue != null) {
                args.Add("query", queryValue);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new ArticleConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of the shop' blogs.
        /// </summary>
        /// <param name="query">
        /// Supported filter parameters:
        /// - `handle`
        /// - `title`
        /// - `updated_at`
        /// - `created_at`
        /// </param>
        public ShopQuery blogs(BlogConnectionDelegate buildQuery,long? first = null,string after = null,long? last = null,string before = null,bool? reverse = null,BlogSortKeys? sortKey = null,string queryValue = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("blogs___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("blogs ");

            Arguments args = new Arguments();

            if (first != null) {
                args.Add("first", first);
            }

            if (after != null) {
                args.Add("after", after);
            }

            if (last != null) {
                args.Add("last", last);
            }

            if (before != null) {
                args.Add("before", before);
            }

            if (reverse != null) {
                args.Add("reverse", reverse);
            }

            if (sortKey != null) {
                args.Add("sortKey", sortKey);
            }

            if (queryValue != null) {
                args.Add("query", queryValue);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new BlogConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// \deprecated Use `paymentSettings` instead
        /// <summary>
        /// The url pointing to the endpoint to vault credit cards.
        /// </summary>
        public ShopQuery cardVaultUrl() {
            Log.DeprecatedQueryField("Shop", "cardVaultUrl", "Use `paymentSettings` instead");

            Query.Append("cardVaultUrl ");

            return this;
        }

        /// <summary>
        /// Find a collection by its handle.
        /// </summary>
        public ShopQuery collectionByHandle(CollectionDelegate buildQuery,string handle,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("collectionByHandle___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("collectionByHandle ");

            Arguments args = new Arguments();

            args.Add("handle", handle);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CollectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of the shop’s collections.
        /// </summary>
        /// <param name="query">
        /// Supported filter parameters:
        /// - `title`
        /// - `collection_type`
        /// - `updated_at`
        /// </param>
        public ShopQuery collections(CollectionConnectionDelegate buildQuery,long? first = null,string after = null,long? last = null,string before = null,bool? reverse = null,CollectionSortKeys? sortKey = null,string queryValue = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("collections___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("collections ");

            Arguments args = new Arguments();

            if (first != null) {
                args.Add("first", first);
            }

            if (after != null) {
                args.Add("after", after);
            }

            if (last != null) {
                args.Add("last", last);
            }

            if (before != null) {
                args.Add("before", before);
            }

            if (reverse != null) {
                args.Add("reverse", reverse);
            }

            if (sortKey != null) {
                args.Add("sortKey", sortKey);
            }

            if (queryValue != null) {
                args.Add("query", queryValue);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new CollectionConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// \deprecated Use `paymentSettings` instead
        /// <summary>
        /// The three-letter code for the currency that the shop accepts.
        /// </summary>
        public ShopQuery currencyCode() {
            Log.DeprecatedQueryField("Shop", "currencyCode", "Use `paymentSettings` instead");

            Query.Append("currencyCode ");

            return this;
        }

        /// <summary>
        /// A description of the shop.
        /// </summary>
        public ShopQuery description() {
            Query.Append("description ");

            return this;
        }

        /// <summary>
        /// A string representing the way currency is formatted when the currency isn’t specified.
        /// </summary>
        public ShopQuery moneyFormat() {
            Query.Append("moneyFormat ");

            return this;
        }

        /// <summary>
        /// The shop’s name.
        /// </summary>
        public ShopQuery name() {
            Query.Append("name ");

            return this;
        }

        /// <summary>
        /// Settings related to payments.
        /// </summary>
        public ShopQuery paymentSettings(PaymentSettingsDelegate buildQuery) {
            Query.Append("paymentSettings ");

            Query.Append("{");
            buildQuery(new PaymentSettingsQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// The shop’s primary domain.
        /// </summary>
        public ShopQuery primaryDomain(DomainDelegate buildQuery) {
            Query.Append("primaryDomain ");

            Query.Append("{");
            buildQuery(new DomainQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// The shop’s privacy policy.
        /// </summary>
        public ShopQuery privacyPolicy(ShopPolicyDelegate buildQuery) {
            Query.Append("privacyPolicy ");

            Query.Append("{");
            buildQuery(new ShopPolicyQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// Find a product by its handle.
        /// </summary>
        public ShopQuery productByHandle(ProductDelegate buildQuery,string handle,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("productByHandle___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("productByHandle ");

            Arguments args = new Arguments();

            args.Add("handle", handle);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new ProductQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of the shop’s product types.
        /// </summary>
        public ShopQuery productTypes(StringConnectionDelegate buildQuery,long first,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("productTypes___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("productTypes ");

            Arguments args = new Arguments();

            args.Add("first", first);

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new StringConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of the shop’s products.
        /// </summary>
        /// <param name="query">
        /// Supported filter parameters:
        /// - `title`
        /// - `product_type`
        /// - `vendor`
        /// - `created_at`
        /// - `updated_at`
        /// - `tag`
        /// </param>
        public ShopQuery products(ProductConnectionDelegate buildQuery,long? first = null,string after = null,long? last = null,string before = null,bool? reverse = null,ProductSortKeys? sortKey = null,string queryValue = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("products___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("products ");

            Arguments args = new Arguments();

            if (first != null) {
                args.Add("first", first);
            }

            if (after != null) {
                args.Add("after", after);
            }

            if (last != null) {
                args.Add("last", last);
            }

            if (before != null) {
                args.Add("before", before);
            }

            if (reverse != null) {
                args.Add("reverse", reverse);
            }

            if (sortKey != null) {
                args.Add("sortKey", sortKey);
            }

            if (queryValue != null) {
                args.Add("query", queryValue);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new ProductConnectionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// The shop’s refund policy.
        /// </summary>
        public ShopQuery refundPolicy(ShopPolicyDelegate buildQuery) {
            Query.Append("refundPolicy ");

            Query.Append("{");
            buildQuery(new ShopPolicyQuery(Query));
            Query.Append("}");

            return this;
        }

        /// \deprecated Use `paymentSettings` instead
        /// <summary>
        /// The shop’s Shopify Payments account id.
        /// </summary>
        public ShopQuery shopifyPaymentsAccountId() {
            Log.DeprecatedQueryField("Shop", "shopifyPaymentsAccountId", "Use `paymentSettings` instead");

            Query.Append("shopifyPaymentsAccountId ");

            return this;
        }

        /// <summary>
        /// The shop’s terms of service.
        /// </summary>
        public ShopQuery termsOfService(ShopPolicyDelegate buildQuery) {
            Query.Append("termsOfService ");

            Query.Append("{");
            buildQuery(new ShopPolicyQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
