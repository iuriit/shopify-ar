namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void CustomerRecoverPayloadDelegate(CustomerRecoverPayloadQuery query);

    public class CustomerRecoverPayloadQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="CustomerRecoverPayloadQuery" /> is used to build queries. Typically
        /// <see cref="CustomerRecoverPayloadQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public CustomerRecoverPayloadQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// List of errors that occurred executing the mutation.
        /// </summary>
        public CustomerRecoverPayloadQuery userErrors(UserErrorDelegate buildQuery) {
            Query.Append("userErrors ");

            Query.Append("{");
            buildQuery(new UserErrorQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
