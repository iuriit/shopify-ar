namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void CustomerResetPayloadDelegate(CustomerResetPayloadQuery query);

    public class CustomerResetPayloadQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="CustomerResetPayloadQuery" /> is used to build queries. Typically
        /// <see cref="CustomerResetPayloadQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public CustomerResetPayloadQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// The customer object which was reset.
        /// </summary>
        public CustomerResetPayloadQuery customer(CustomerDelegate buildQuery) {
            Query.Append("customer ");

            Query.Append("{");
            buildQuery(new CustomerQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of errors that occurred executing the mutation.
        /// </summary>
        public CustomerResetPayloadQuery userErrors(UserErrorDelegate buildQuery) {
            Query.Append("userErrors ");

            Query.Append("{");
            buildQuery(new UserErrorQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
