namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void CheckoutCompleteWithCreditCardPayloadDelegate(CheckoutCompleteWithCreditCardPayloadQuery query);

    public class CheckoutCompleteWithCreditCardPayloadQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="CheckoutCompleteWithCreditCardPayloadQuery" /> is used to build queries. Typically
        /// <see cref="CheckoutCompleteWithCreditCardPayloadQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public CheckoutCompleteWithCreditCardPayloadQuery(StringBuilder query) {
            Query = query;
        }

        /// <summary>
        /// The checkout on which the payment was applied.
        /// </summary>
        public CheckoutCompleteWithCreditCardPayloadQuery checkout(CheckoutDelegate buildQuery) {
            Query.Append("checkout ");

            Query.Append("{");
            buildQuery(new CheckoutQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// A representation of the attempted payment.
        /// </summary>
        public CheckoutCompleteWithCreditCardPayloadQuery payment(PaymentDelegate buildQuery) {
            Query.Append("payment ");

            Query.Append("{");
            buildQuery(new PaymentQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of errors that occurred executing the mutation.
        /// </summary>
        public CheckoutCompleteWithCreditCardPayloadQuery userErrors(UserErrorDelegate buildQuery) {
            Query.Append("userErrors ");

            Query.Append("{");
            buildQuery(new UserErrorQuery(Query));
            Query.Append("}");

            return this;
        }
    }
    }
