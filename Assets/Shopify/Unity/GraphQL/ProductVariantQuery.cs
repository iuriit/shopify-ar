namespace Shopify.Unity.GraphQL {
    using System;
    using System.Text;
    using System.Collections.Generic;
    using Shopify.Unity.SDK;

    public delegate void ProductVariantDelegate(ProductVariantQuery query);

    /// <summary>
    /// A product variant represents a different version of a product, such as differing sizes or differing colors.
    /// </summary>
    public class ProductVariantQuery {
        private StringBuilder Query;

        /// <summary>
        /// <see cref="ProductVariantQuery" /> is used to build queries. Typically
        /// <see cref="ProductVariantQuery" /> will not be used directly but instead will be used when building queries from either
        /// <see cref="QueryRootQuery" /> or <see cref="MutationQuery" />.
        /// </summary>
        public ProductVariantQuery(StringBuilder query) {
            Query = query;
        }

        /// \deprecated Use `availableForSale` instead
        /// <summary>
        /// Indicates if the product variant is in stock.
        /// </summary>
        public ProductVariantQuery available() {
            Log.DeprecatedQueryField("ProductVariant", "available", "Use `availableForSale` instead");

            Query.Append("available ");

            return this;
        }

        /// <summary>
        /// Indicates if the product variant is available for sale.
        /// </summary>
        public ProductVariantQuery availableForSale() {
            Query.Append("availableForSale ");

            return this;
        }

        /// <summary>
        /// The compare at price of the variant. This can be used to mark a variant as on sale, when `compareAtPrice` is higher than `price`.
        /// </summary>
        public ProductVariantQuery compareAtPrice() {
            Query.Append("compareAtPrice ");

            return this;
        }

        /// <summary>
        /// Globally unique identifier.
        /// </summary>
        public ProductVariantQuery id() {
            Query.Append("id ");

            return this;
        }

        /// <summary>
        /// Image associated with the product variant.
        /// </summary>
        /// <param name="maxWidth">
        /// Image width in pixels between 1 and 2048
        /// </param>
        /// <param name="maxHeight">
        /// Image height in pixels between 1 and 2048
        /// </param>
        /// <param name="crop">
        /// If specified, crop the image keeping the specified region
        /// </param>
        /// <param name="scale">
        /// Image size multiplier retina displays. Must be between 1 and 3
        /// </param>
        public ProductVariantQuery image(ImageDelegate buildQuery,long? maxWidth = null,long? maxHeight = null,CropRegion? crop = null,long? scale = null,string alias = null) {
            if (alias != null) {
                ValidationUtils.ValidateAlias(alias);

                Query.Append("image___");
                Query.Append(alias);
                Query.Append(":");
            }

            Query.Append("image ");

            Arguments args = new Arguments();

            if (maxWidth != null) {
                args.Add("maxWidth", maxWidth);
            }

            if (maxHeight != null) {
                args.Add("maxHeight", maxHeight);
            }

            if (crop != null) {
                args.Add("crop", crop);
            }

            if (scale != null) {
                args.Add("scale", scale);
            }

            Query.Append(args.ToString());

            Query.Append("{");
            buildQuery(new ImageQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// The product variant’s price.
        /// </summary>
        public ProductVariantQuery price() {
            Query.Append("price ");

            return this;
        }

        /// <summary>
        /// The product object that the product variant belongs to.
        /// </summary>
        public ProductVariantQuery product(ProductDelegate buildQuery) {
            Query.Append("product ");

            Query.Append("{");
            buildQuery(new ProductQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// List of product options applied to the variant.
        /// </summary>
        public ProductVariantQuery selectedOptions(SelectedOptionDelegate buildQuery) {
            Query.Append("selectedOptions ");

            Query.Append("{");
            buildQuery(new SelectedOptionQuery(Query));
            Query.Append("}");

            return this;
        }

        /// <summary>
        /// The SKU (Stock Keeping Unit) associated with the variant.
        /// </summary>
        public ProductVariantQuery sku() {
            Query.Append("sku ");

            return this;
        }

        /// <summary>
        /// The product variant’s title.
        /// </summary>
        public ProductVariantQuery title() {
            Query.Append("title ");

            return this;
        }

        /// <summary>
        /// The weight of the product variant in the unit system specified with `weight_unit`.
        /// </summary>
        public ProductVariantQuery weight() {
            Query.Append("weight ");

            return this;
        }

        /// <summary>
        /// Unit of measurement for weight.
        /// </summary>
        public ProductVariantQuery weightUnit() {
            Query.Append("weightUnit ");

            return this;
        }
    }
    }
