﻿using System.Collections.Generic;
using System.Linq;
using Shopify.Examples.LineItems;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Shopify.Examples.Panels {

    public class ProductsManager : MonoBehaviour {

//		public AnimationManager productPageAnimation;
//		public ProductManager productPage;

		public ItemManager ProductTemplate;

		private List<GameObject> _lineItems = new List<GameObject>();

        public ScrollRect ScrollView;
        public RectTransform Content;

        private RectTransform RectTransform;

        private void Start() {
            RectTransform = GetComponent<RectTransform>();
     //       ViewCartButton.onClick.AddListener(() => OnViewCart.Invoke());
                                                                                     //       ClosePanelButton.onClick.AddListener(() => OnClosePanel.Invoke());
        }

        public void AddProduct(Shopify.Unity.Product product) {
            // Create instance of the template
            var instance = Instantiate(ProductTemplate);
            // Need to set transform so that scrolling works properly
            instance.transform.SetParent(Content, false);
            // Pass in the product to set the line item attributes
            instance.SetCurrentProduct(product);

            // When the instance is clicked, dispatch up the event to change the view to the product
/*			instance.OnClick.AddListener(() => {
				productPageAnimation.StartAnimation ();
				productPage.ShowProduct(product);
			});
			*/

            // Add to our list of line items, as we need to iterate them later to adjust properties 
            // such as the separator element visibility
			_lineItems.Add(instance.gameObject);
        }

		public void Clear() {
			foreach (var product in _lineItems) {
				Destroy(product);
			}
			_lineItems.Clear ();
		}
    }
}