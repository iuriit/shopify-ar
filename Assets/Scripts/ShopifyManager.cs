﻿using System.Collections.Generic;
using Components;
using Shopify.Examples.Helpers;
using Shopify.Examples.Panels;
using Shopify.Unity;
using Shopify.Unity.SDK;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public enum Category
{
	Men = 0,
	Women = 1,
	Accessories = 2,
	Tech = 3,
	MobileCase = 4,
	Art = 5,
	Bag= 6,
	Other = 7,
}

public class ShopifyManager : MonoBehaviour {
    public ProductsManager ProductsPanel;
	public ProductManager ProductPanel;
	public CheckoutManager CartPanel;
	public GameObject progresspanel;

	public Text logText;
	private Dictionary<string, List<Product>> productsDictionary = new Dictionary<string, List<Product>> ();

    // Credentials for access to Shopify store 
    // For more information about getting an access token visit
    // https://help.shopify.com/api/storefront-api/getting-started#authentication

    public string AccessToken = "8f40b2ede3e02be97a81ac29cfabc6e0";
    public string ShopDomain = "unity-buy-sdk.myshopify.com";

    private void Start() {
		// Begin by initializing ShopifyHelper, to make connection to shop
		ShopifyHelper.Init (AccessToken, ShopDomain);
		/*
		ShopifyHelper.FetchCollections (
			delegate (List<Collection> collections) {
				// collections is a List<Colletion>
				Debug.Log("Your shop has " + collections.Count + " collections");
				Debug.Log("===================================================");

				foreach(Collection collection in collections) {
					productsDictionary[collection.title ()] = (List<Product>) collection.products ();
				}
			},
			delegate {
				Debug.LogError ("Could not find collections");
			}
		);*/
		// Handles click of the add to cart button, which transitions to the cart view
		ProductPanel.OnAddProductToCart.AddListener(CartPanel.AddToCart);

	}

	public void OnCategorySelect(string cat) {
		if (productsDictionary.ContainsKey (cat)) {
			SetProducts (productsDictionary [cat]);
			return;
		}

		progresspanel.SetActive (true);
		logText.text = "Fetching...";
	
		ShopifyHelper.FetchProducts (
			delegate (List<Product> products) {
				productsDictionary[cat] = products;
				logText.text = "Finished" + products.Count;
				progresspanel.SetActive (false);
			
				SetProducts (products);
			},
			delegate {
				logText.text = "error during fetching!";
				Debug.LogError ("Could not find products");
			}
		);
	}

	private void SetProducts(List<Product> products) {
		ProductsPanel.Clear();
		foreach (var product in products) {
			// For each of the products received, add them to the products panel
			ProductsPanel.AddProduct (product);
		}
	}
}