﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

	public string name;
	public int type;
	public Sprite sprite;
	public float price;
	public string time;
	public float review;

}
